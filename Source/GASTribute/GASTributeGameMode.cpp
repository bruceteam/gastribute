// Copyright Epic Games, Inc. All Rights Reserved.

#include "GASTributeGameMode.h"
#include "GASTributeCharacter.h"
#include "UObject/ConstructorHelpers.h"

AGASTributeGameMode::AGASTributeGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
