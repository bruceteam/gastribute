// Fill out your copyright notice in the Description page of Project Settings.


#include "GASTribute/Enemy/EnemySpawner.h"
#include "Components/StaticMeshComponent.h"
#include "Character/GASCharacter.h"
#include "Components/SphereComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "AI/NavigationSystemBase.h"
#include "DrawDebugHelpers.h"
#include "NavigationSystem.h"

DEFINE_LOG_CATEGORY_STATIC(LogEnemySpawner, All, All);

AEnemySpawner::AEnemySpawner()
{
	PrimaryActorTick.bCanEverTick = false;

	MeshSpawner = CreateDefaultSubobject<UStaticMeshComponent>("MeshSpawner");
	MeshSpawner->SetupAttachment(GetRootComponent());



	SphereCollisionSpawner = CreateDefaultSubobject<USphereComponent>("SphereCollisionSpawner");
	SphereCollisionSpawner->InitSphereRadius(1000.0f);
	SphereCollisionSpawner->SetupAttachment(MeshSpawner);
	SphereCollisionSpawner->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
}


void AEnemySpawner::BeginPlay()
{
	Super::BeginPlay();

	MeshSpawner->SetMaterial(0, EnemyColor);
}

void AEnemySpawner::NotifyActorBeginOverlap(AActor* OtherActor)
{
	if (AGASCharacter* GASC = Cast<AGASCharacter>(OtherActor))
	{
		if (OtherActor->ActorHasTag("Player"))
		{
			if (ShowDebug)
				UE_LOG(LogEnemySpawner, Warning, TEXT("Overlapping Char - %s"), *GASC->GetName());

			GetWorld()->GetTimerManager().ClearTimer(TimerToRespawn);
			SpawnEnemy();
			GetWorld()->GetTimerManager().SetTimer(TimerToRespawn, this, &AEnemySpawner::EnemySpawnTick, TimeToRespawnEnemies, true, TimeToRespawnEnemies);
		}
	}
}

void AEnemySpawner::EnemySpawnTick()
{
	TArray<AActor*> Actors;
	SphereCollisionSpawner->GetOverlappingActors(Actors, AGASCharacter::StaticClass());

	if (Actors.Num() > 0)
	{
		for (AActor* Char : Actors)
		{
			if (Char->ActorHasTag("Player"))
			{
				SpawnEnemy();
				Actors.Empty();
				return;
			}
			else
				continue;
		}
	}
	else
	{
		GetWorld()->GetTimerManager().ClearTimer(TimerToRespawn);
	}
}

void AEnemySpawner::SpawnEnemy()
{
	UNavigationSystemV1* navSystem = UNavigationSystemV1::GetCurrent(GetWorld());
	if (navSystem)
	{
		FNavLocation ResultLoc;
		bool Success = navSystem->GetRandomReachablePointInRadius(GetActorLocation(), 1000.0f, ResultLoc);
		if (Success)
		{
			if (ShowDebug)
				DrawDebugSphere(GetWorld(), ResultLoc, 20.0f, 20, FColor::Red, false, 3.0f);
		}

		if (EnemyToSpawn)
		{
			const FTransform SpawnTransform(FRotator::ZeroRotator, ResultLoc);
			ACharacter* EnemyChar = GetWorld()->SpawnActor<ACharacter>(EnemyToSpawn, SpawnTransform);
		}
	}
}


