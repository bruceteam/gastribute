// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EnemySpawner.generated.h"

class USphereComponent;

UCLASS()
class GASTRIBUTE_API AEnemySpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	AEnemySpawner();

	UPROPERTY(BlueprintReadWrite, EditAnywhere,Category = "All")
		UStaticMeshComponent* MeshSpawner;
	UPROPERTY(BlueprintReadWrite, EditAnywhere,Category = "All")
		USphereComponent* SphereCollisionSpawner;
	UPROPERTY(BlueprintReadWrite, EditAnywhere,Category = "All")
		TSubclassOf<ACharacter> EnemyToSpawn;
	UPROPERTY(BlueprintReadWrite, EditAnywhere,Category = "All")
		UMaterialInterface* EnemyColor;
	UPROPERTY(BlueprintReadWrite, EditAnywhere,Category = "All")
		float TimeToRespawnEnemies = 5.0f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "All")
		bool ShowDebug = true;

	FTimerHandle TimerToRespawn;

protected:
	virtual void BeginPlay() override;
	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

	void SpawnEnemy();
	void EnemySpawnTick();

};
