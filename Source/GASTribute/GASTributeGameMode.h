// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GASTributeGameMode.generated.h"

UCLASS(minimalapi)
class AGASTributeGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGASTributeGameMode();
};



