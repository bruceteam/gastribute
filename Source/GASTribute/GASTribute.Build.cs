// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class GASTribute : ModuleRules
{
	public GASTribute(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", 
			"CoreUObject",
			"Engine",
			"InputCore",
			"HeadMountedDisplay", 
			"EnhancedInput",
            "AIModule",
            "NavigationSystem",
            "GameplayAbilities",			//add this modules
			"GameplayTags",					//add this modules
			"GameplayTasks" });             //add this modules
    }
}
